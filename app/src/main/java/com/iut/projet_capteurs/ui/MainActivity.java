package com.iut.projet_capteurs.ui;

import androidx.appcompat.app.AppCompatActivity;
import com.iut.projet_capteurs.R;
import com.iut.projet_capteurs.databinding.ActivityMainBinding;
import com.iut.projet_capteurs.model.GPX;

import android.content.Intent;
import android.os.Bundle;

import java.io.InputStream;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.mapButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, MapsActivity.class);
            startActivity(intent);
        });
    }
}