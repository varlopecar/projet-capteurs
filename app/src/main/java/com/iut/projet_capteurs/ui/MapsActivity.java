package com.iut.projet_capteurs.ui;

import androidx.fragment.app.FragmentActivity;

import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.PolylineOptions;
import com.iut.projet_capteurs.R;
import com.iut.projet_capteurs.databinding.ActivityMapsBinding;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.iut.projet_capteurs.model.GPX;
import com.iut.projet_capteurs.pdr.DeviceAttitudeHandler;
import com.iut.projet_capteurs.pdr.StepDetectionHandler;
import com.iut.projet_capteurs.pdr.StepPositionHandler;
import com.iut.projet_capteurs.viewer.GoogleMapTracer;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    InputStream is;

    StepDetectionHandler stepDetectionHandler;

    StepPositionHandler stepPositionHandler;

    DeviceAttitudeHandler deviceAttitudeHandler;

    GoogleMapTracer googleMapTracer;

    private LatLng lastKnownLocation;

    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        com.iut.projet_capteurs.databinding.ActivityMapsBinding binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        stepDetectionHandler = new StepDetectionHandler(sensorManager);
        deviceAttitudeHandler = new DeviceAttitudeHandler(sensorManager);
        stepPositionHandler = new StepPositionHandler();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng grenoble = new LatLng(45.188, 5.725);
        googleMap.addMarker(new MarkerOptions().position(grenoble).title("Marker in Grenoble"));
        googleMap.moveCamera(CameraUpdateFactory.zoomTo(13));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(grenoble));

        stepDetectionHandler.start();
        stepDetectionHandler.setListener(new StepDetectionHandler.StepDetectionListener() {
            @Override
            public void newStep(float stepSize) {
                Log.d("StepDetectionHandler", "New step: " + stepSize);
            }
        });

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                lastKnownLocation = latLng;
                if (googleMapTracer == null) {
                    googleMapTracer = new GoogleMapTracer(googleMap, lastKnownLocation);
                    googleMapTracer.newSegment(lastKnownLocation);
                }
                updatePolyline(lastKnownLocation);
            }
        });

        deviceAttitudeHandler.start();
        deviceAttitudeHandler.setOnDeviceAttitudeChangeListener(new DeviceAttitudeHandler.OnDeviceAttitudeChangeListener() {
            @Override
            public void onDeviceAttitudeChange(float[] rotationMatrix, float[] orientationAngles) {
                // Update the camera bearing based on the device orientation
                if (googleMap != null) {
                    CameraPosition currentCameraPosition = googleMap.getCameraPosition();
                    CameraPosition newCameraPosition = CameraPosition.builder(currentCameraPosition)
                            .bearing((float) Math.toDegrees(orientationAngles[0]))
                            .build();
                    float bearing = (float) Math.toDegrees(orientationAngles[0]);
                    Log.d("MapsActivity", "onDeviceAttitudeChange: " + bearing);
                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(newCameraPosition));
                    LatLng latLng = new LatLng(45.188, 5.725);
                    Location location = new Location(latLng.toString());
                    stepPositionHandler.setCurrentLocation(location);
                    stepPositionHandler.computeNextStep(0.5f, bearing);
                }
            }
        });
    }

    private void updatePolyline(LatLng newLocation) {
        if (googleMapTracer != null) {
            googleMapTracer.newSegment(newLocation);
        }
    }

    private LatLng getLastKnownLocation() {
        if (lastKnownLocation == null) {
            return null;
        }
        return new LatLng(lastKnownLocation.latitude, lastKnownLocation.longitude);
    }

    public void trace() {
        try {
            is = getAssets().open("simple.gpx");
            GPX gpxparse = GPX.parse(is);

            for (GPX.Track tracks : gpxparse) {
                for (GPX.TrackSegment trackSegments : tracks) {
                    PolylineOptions polylineOptions = new PolylineOptions();
                    for (GPX.TrackPoint trackPoints : trackSegments) {
                        polylineOptions.add(trackPoints.position);
                        System.out.println(trackPoints.position);
                    }
                    googleMap.addPolyline(polylineOptions);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}