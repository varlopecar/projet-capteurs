package com.iut.projet_capteurs.pdr;

import android.location.Location;
import android.util.Log;

public class StepPositionHandler {
    private Location mCurrentLocation;

    public void StepPositioningHandler(Location currentLocation) {
        mCurrentLocation = currentLocation;
    }

    public Location getCurrentLocation() {
        return mCurrentLocation;
    }

    public void setCurrentLocation(Location currentLocation) {
        mCurrentLocation = currentLocation;
    }

    public void computeNextStep(float stepSize, float bearing) {
        double dx = stepSize * Math.cos(bearing);
        double dy = stepSize * Math.sin(bearing);

        Location newLocation = new Location(mCurrentLocation);
        newLocation.setLatitude(mCurrentLocation.getLatitude() + dx);
        newLocation.setLongitude(mCurrentLocation.getLongitude() + dy);

        Log.d("StepPositionHandler", "New location: " + newLocation);
    }
}
