package com.iut.projet_capteurs.pdr;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class DeviceAttitudeHandler implements SensorEventListener {
    private SensorManager sensorManager;
    private Sensor rotationVectorSensor;
    private boolean isListening;

    private OnDeviceAttitudeChangeListener onDeviceAttitudeChangeListener;

    public DeviceAttitudeHandler(SensorManager sensorManager) {
        this.sensorManager = sensorManager;
        this.rotationVectorSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        this.isListening = false;
    }

    public void start() {
        if (!isListening) {
            sensorManager.registerListener(this, rotationVectorSensor, SensorManager.SENSOR_DELAY_NORMAL);
            isListening = true;
        }
    }

    public void stop() {
        if (isListening) {
            sensorManager.unregisterListener(this);
            isListening = false;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            // Handle the rotation vector data here
            float[] rotationMatrix = new float[9];
            SensorManager.getRotationMatrixFromVector(rotationMatrix, event.values);
            // Extract the device's orientation angles from the rotation matrix
            float[] orientationAngles = new float[3];
            SensorManager.getOrientation(rotationMatrix, orientationAngles);
            // Do something with the orientation angles (e.g. update UI)
            float yaw = orientationAngles[0];
            float pitch = orientationAngles[1];
            float roll = orientationAngles[2];
            // convert to radian degrees
            yaw = (float) Math.toDegrees(yaw);
            pitch = (float) Math.toDegrees(pitch);
            roll = (float) Math.toDegrees(roll);
            if (onDeviceAttitudeChangeListener != null) {
                onDeviceAttitudeChangeListener.onDeviceAttitudeChange(rotationMatrix, orientationAngles);
            }
            Log.d("DeviceAttitudeHandler", "onSensorChanged: " + yaw + " " + pitch + " " + roll);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do nothing
    }

    public void setOnDeviceAttitudeChangeListener(OnDeviceAttitudeChangeListener listener) {
        onDeviceAttitudeChangeListener = listener;
    }

    public interface OnDeviceAttitudeChangeListener {
        void onDeviceAttitudeChange(float[] rotationMatrix, float[] orientationAngles);
    }
}
