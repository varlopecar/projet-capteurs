package com.iut.projet_capteurs.pdr;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class StepDetectionHandler implements SensorEventListener {
    private static final String TAG = "StepDetectionHandler";

    private SensorManager sensorManager;
    private Sensor linearAccelerationSensor;
    private boolean isListening = false;
    private float[] zValues = new float[5];
    private int zIndex = 0;
    private StepDetectionListener listener;

    public StepDetectionHandler(SensorManager sensorManager) {
        this.sensorManager = sensorManager;
        this.linearAccelerationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
    }

    public void start() {
        if (!isListening) {
            sensorManager.registerListener(this, linearAccelerationSensor, SensorManager.SENSOR_DELAY_NORMAL);
            isListening = true;
        }
    }

    public void stop() {
        if (isListening) {
            sensorManager.unregisterListener(this);
            isListening = false;
        }
    }

    public void setListener(StepDetectionListener listener) {
        this.listener = listener;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            float z = event.values[2];

            // Apply moving average on z values
            float sumZ = 0;
            for (int i = 0; i < zValues.length; i++) {
                sumZ += zValues[i];
            }
            zValues[zIndex] = z;
            zIndex = (zIndex + 1) % zValues.length;
            z = sumZ / zValues.length;

            // Detect step
            if (z > 1.5) {
                if (listener != null) {
                    listener.newStep(0.8f); // 0.8m step size
                }
                Log.d(TAG, "Step detected");
            }
            Log.d(TAG, "Z acceleration: " + z);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do nothing
    }

    public interface StepDetectionListener {
        void newStep(float stepSize);
    }
}

