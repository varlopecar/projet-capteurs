package com.iut.projet_capteurs.viewer;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.GoogleMap;

public class GoogleMapTracer {
    private GoogleMap map;
    private Polyline currentPolyline;
    private Marker startMarker;
    private Marker endMarker;

    public GoogleMapTracer(GoogleMap map, LatLng lastKnownLocation) {
        this.map = map;
    }

    public void newSegment(LatLng point) {
        currentPolyline = map.addPolyline(new PolylineOptions()
                .clickable(true)
                .add(point));
        startMarker = map.addMarker(new MarkerOptions().position(point));
    }

    public void newPoint(LatLng point) {
        if (currentPolyline == null) {
            PolylineOptions options = new PolylineOptions()
                    .clickable(false)
                    .color(0xFF0000FF)
                    .width(5)
                    .add(point);
            currentPolyline = map.addPolyline(options);
            startMarker = map.addMarker(new MarkerOptions().position(point));
        } else {
            currentPolyline.getPoints().add(point);
        }
        endMarker = map.addMarker(new MarkerOptions().position(point));
    }
}
